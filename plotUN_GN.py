from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan, arctan
import sys
from sys import argv, exit

group = UN
Repr = UN.Repr
d = UN.d

quiet = False
ntails = 2

gridfactor = 3

thetamax = 10

if len(sys.argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)

N = int(sys.argv[1])
nmax = int(sys.argv[2])

# Location of pole in theta plane, for each of the channels
thetap = [1j*pi*(1-2/N), 1j*pi*(1-2/N), None, 2*1j*pi/N]
# Mass in antisymmetric channel
mass = sqrt(np.real(sfuntheta(thetap[3])))

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# params = {'legend.fontsize': 8, 'lines.markersize':2.5, 'lines.marker':"o"}
# plt.rcParams.update(params)
plt.style.use('ggplot')

reps = [Repr.ANTI]
mb = [mass]
bsList = [BoundState(mb[i], N, group, reps[i]) for i in range(len(mb))]

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

S = Smatrix(bsList, N, nmax, group, ntails, quiet=quiet)

obj = np.zeros(S.size)
# Maximize coupling to antisymmetric bound state
obj[0] = -1

o = Optimizer(grid, S)
res = o.optimize(obj)
gSq = -res['primal objective']
print("Maximum g:", gSq)

# if tail:
    # print("tail coefficients: {}".format(res['x'][-2:]))


#############################
# Plot at physical energy
############################

thetaList = np.linspace(0.01, thetamax, 200)
ylist = yfunthetaRe(thetaList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(op, res['x']) for op in ops], dtype=complex)).transpose()
fAnalytic = array([SUN_GN(th, N) for th in thetaList]).transpose()

# Check that solution is crossin symmetric
# print("Check that numeric solution is crossin symmetric")
for y in ylist:
    # print(y)
    # print(abs(y))
    S1 = np.dot(S.linOp(y), res['x'])
    S2 = np.dot(d(N), np.dot(S.linOp(-y),res['x']))
    np.testing.assert_almost_equal(S1, S2)

print("Checking that analytic solution is crossin symmetric...")
for th in thetaList:
    f1 = SUN_GN(th, N)
    f2 = np.dot(d(N), SUN_GN(1j*pi-th, N))
    np.testing.assert_almost_equal(f1, f2)

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

for i, rep in enumerate(list(Repr)):
    ax[i].plot(thetaList, -np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    ax[i].plot(thetaList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(fAnalytic[i]), label=r"$|S|^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=4, prop={'size':6})

fig.suptitle(r"$U(N)$ Gross-Neveu S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}, ntails={}".format(N, nmax, gridsize,ntails))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("SUN_GN_N={}.pdf".format(N))
plt.clf()


#############################
# Plot at imaginary theta
############################

print("theta pole:", thetap)

imthList = np.linspace(0.01, pi-0.1, 100)
ylist = yfuntheta(1j*imthList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(S.linOp(y), res['x']) for y in ylist], dtype=complex)).transpose()
fAnalytic = array([SUN_GN(th, N) for th in 1j*imthList]).transpose()

# Multiply by zeros the singlet, adjoint and antisymmetric components
for i in range(len(Repr)):
    if thetap[i] != None:
        f[i] *= 1j*(1j*imthList-thetap[i])
        fAnalytic[i] *= 1j*(1j*imthList-thetap[i])

plt.figure(2)

# fig, ax  = plt.subplots(len(Repr),1,sharex=True)
# fig.set_figheight(8)
# fig.set_figwidth(5)

# Legend positions
loc = [1,4,4,1]


for i, rep in enumerate(list(Repr)):
    # ax[i].plot(imthList, -np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
    # ax[i].plot(imthList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    # ax[i].plot(imthList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    # ax[i].plot(imthList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    # ax[i].legend(loc=loc[i], prop={'size':6})
    # ax[i].set_ylabel(r"$S_{{\rm {}}} \times \prod (\theta-\theta_0)$".format(str(rep)))

    plt.plot(imthList, -np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
    plt.plot(imthList, np.real(fAnalytic[i]))# , label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

plt.legend(loc=loc[i], prop={'size':6})
plt.ylabel(r"$S \times \prod (\theta-\theta_p)$")

# fig.suptitle(r"$U(N)$ Gross-Neveu S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}, ntails={}".format(N, nmax, gridsize, ntails))
fig.suptitle(r"$U(N)$ Gross-Neveu S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}, ntails={}".format(N, nmax, gridsize, ntails))
plt.title(r"$U(N)$ Gross-Neveu S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}, ntails={}".format(N, nmax, gridsize, ntails))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$Im \theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("SUN_GN_N={}_imTheta.pdf".format(N))
