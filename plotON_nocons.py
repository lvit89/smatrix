from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit

group = ON
Repr = ON.Repr
d = ON.d

coeff = [2.14921413612855,2.5188105870975455,0.33999698500688896]
coeff = [1.3908870202120307,-1.7702941015335816,-0.20776711467220538]
coeff = -array([-1.0846744966783002,0.8173109270073121,0.6839557991889438])

quiet = False
ntails = 2

thetamax = 10

# Number of points on the unitarity grid over number of states
gridfactor = 3

if len(sys.argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)

N = int(sys.argv[1])
nmax = int(sys.argv[2])

# Location of zero in theta strip (it is inside strip only for N >= 5)
theta0 = 1j*lambdaON(N)
# Location of zero in y plane
y0 = yfuntheta(theta0)

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)


rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# params = {'legend.fontsize': 8, 'lines.markersize':2.5, 'lines.marker':"o"}
# plt.rcParams.update(params)
plt.style.use('ggplot')

reps = []
bsList = []

S = Smatrix(bsList, N, nmax, group, ntails, quiet=quiet)

obj = np.einsum("i,ij -> j", coeff, S.linOp(y=0))

# XXX maximize UV perturbative coupling
# obj = np.zeros(S.size)
# obj[-3] = 1.57
# obj[-1] = 1.22

# Impose no constraints
cons = []

o = Optimizer(grid, S)
res = o.optimize(obj, cons=cons)
print("Maximum obj:", res['primal objective'])


if ntails > 0:
    print("Tail coefficients:", res['x'][-S.lentails:])


#############################
# Plot at physical energy
############################

thetaList = np.linspace(0.01, thetamax, 100)
ylist = yfunthetaRe(thetaList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(op, res['x']) for op in ops], dtype=complex)).transpose()
fAnalytic = array([SON(th, N) for th in thetaList]).transpose()

# "Unitarity saturation"
sat = np.absolute(f).flatten().mean()
print("sat: ", sat)


# Check that solution is crossin symmetric
for y in ylist:
    S1 = np.dot(S.linOp(y), res['x'])
    S2 = np.dot(d(N), np.dot(S.linOp(-y),res['x']))
    np.testing.assert_almost_equal(S1, S2)

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

for i, rep in enumerate(list(Repr)):
    ax[i].plot(thetaList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    ax[i].plot(thetaList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(fAnalytic[i]), label=r"$|S|^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=4, prop={'size':6})

fig.suptitle(r"$\sigma$-model S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}, saturation={}".format(N, nmax, gridsize, sat))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("SON_nocons_N={}.pdf".format(N))
plt.clf()


#############################
# Plot at imaginary theta
############################


imthList = np.linspace(0.01, pi-0.01, 100)
ylist = yfuntheta(1j*imthList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(S.linOp(y), res['x']) for y in ylist], dtype=complex)).transpose()
fAnalytic = array([SON(th, N) for th in 1j*imthList]).transpose()

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

# Legend positions
loc = [1,1,4]


for i, rep in enumerate(list(Repr)):
    ax[i].plot(imthList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
    # ax[i].plot(imthList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    ax[i].plot(imthList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    # ax[i].plot(imthList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=loc[i], prop={'size':6})
    ax[i].set_ylabel(r"$S_{{\rm {}}}$".format(str(rep)))

fig.suptitle(r"$O(N) \sigma$-model S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}".format(N, nmax, gridsize))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$Im \theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("SON_nocons_N={}_imTheta.pdf".format(N))
