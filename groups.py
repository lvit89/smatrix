import numpy as np
from numpy import array, pi, exp, log, sqrt
from enum import Enum
from analytic import *
import scipy

class Group():
    pass

class ON(Group):

    def __init__(self, N):

        self.N = N

        # Allowed tensor structures
        self.tensors = ["s1", "s2", "s3"]
        # Allowed independent tensor structures
        self.indepTensors = ["s1", "s2"]

        # Number of independent components
        self.leneven = 2
        self.lenodd = 1

        # Crossing symmetry matrix
        # self.d = array([[0, 0, (1+N)/2, (1-N)/2],
                      # [0, 0, 1/2, 1/2],
                      # [1/N, (-1+N)/N, 0, 0],
                      # [-1/N, (1+N)/N, 0, 0]])

        # Matrix to go from (s1, s2, s3) representation to (SING, ANTI, SYM)
        self.toIrrep = array([[N, 1., 1.],
                            [0., 1, -1.],
                            [0, 1, 1]]
                            )

# XXX CHECK
        self.d = array([[1/N, -N/2+1/2, (N+1)/2-1/N],
                        [-1/N,1/2,1/2+1/N],
                        [1/N,1/2,1/2-1/N]])

    class Irrep(Enum):
        SING = 1
        ANTI = 2
        SYM = 3
        def __str__(self):
            return ["Sing", "Anti", "Sym"][self.value-1]


    def tMat(self, n):
        """ Tensor matrix in the (s1, s2, s3) representation,
        which takes into account crossing symmetry
        n: order of the monomial, or parity under y <-> -y
        """
    # NOTE the second row is only for n even
        if n%2==0:
            return np.array([[1., 0, 1],
                            [0, 1, 0]]).transpose()
        elif n%2==1:
            return np.array([[1., 0, -1]]).transpose()


# XXX Check
    def boundStateVec(rep):
        if rep==ON.Repr.SING:
            return array([1,0,0])
        elif rep==ON.Repr.ANTI:
            return array([0,-1,0])
        elif rep==ON.Repr.SYM:
            return array([0,0,1])
        else:
            raise RuntimeError("Unknown representation code")

    # def nullSpaceOdd(N):
        # """ Crossing invariant vector for function odd in y """
        # return array([1-N, -1., 1.]).reshape((3,1))

    # def nullSpaceEven(N):
        # """ Crossing invariant vector for function even in y """
        # return array([[(2+N)/2,0.,1.], [-N/2,1.,0.]]).transpose()

    # def nullSpace(N,n):
        # if n%2==0:
            # return ON.nullSpaceEven(N)
        # elif n%2==1:
            # return ON.nullSpaceOdd(N)

class ON2(Group):
    """ Class II O(N) models in Vieira's paper """

    def __init__(self, N):

        self.N = N

        # Allowed tensor structures
        self.tensors = ["s1", "s3"]
        # Allowed independent tensor structures
        self.indepTensors = ["s1"]

        # Number of independent components
        self.leneven = 1
        self.lenodd = 1

        # Crossing symmetry matrix
        # self.d = array([[0, 0, (1+N)/2, (1-N)/2],
                      # [0, 0, 1/2, 1/2],
                      # [1/N, (-1+N)/N, 0, 0],
                      # [-1/N, (1+N)/N, 0, 0]])

        # Matrix to go from (s1, s3) representation to (SING, ANTI, SYM)
        self.toIrrep = array([[N, 1.],
                            [0., -1.],
                            [0, 1]]
                            )

# XXX CHECK
        self.d = array([[1/N, -N/2+1/2, (N+1)/2-1/N],
                        [-1/N,1/2,1/2+1/N],
                        [1/N,1/2,1/2-1/N]])

    class Irrep(Enum):
        SING = 1
        ANTI = 2
        SYM = 3
        def __str__(self):
            return ["Sing", "Anti", "Sym"][self.value-1]


    def tMat(self, n):
        """ Tensor matrix in the (s1, s3) representation,
        which takes into account crossing symmetry
        n: order of the monomial, or parity under y <-> -y
        """
        return np.array([[1., (-1)**n]]).transpose()



class UN(Group):
    """ Group UN with with constraints r1 = r2 = 0 """

    def __init__(self, N):

        self.parityInv = True

        # Allowed tensor structures
        self.tensors = ["t1", "u1", "t2", "u2"]
        # Allowed independent tensor structures
        self.indepTensors = ["t1", "t2"]

        self.N = N
        # Number of independent components
        self.leneven = 2
        self.lenodd = 2


        # Crossing symmetry matrix
        self.d = array([[0, 0, (1+N)/2, (1-N)/2],
                      [0, 0, 1/2, 1/2],
                      [1/N, (-1+N)/N, 0, 0],
                      [-1/N, (1+N)/N, 0, 0]])

        # Matrix to go from (t1, u1, t2, u2) representation to (SING, ADJ, SYM, ANTI)
        self.toIrrep = array([[1., 0., N, 0.],
                            [1, 0, 0, 0],
                            [0, 1, 0, 1],
                            [0, 1, 0, -1]])

    def tMat(self, n):
        """ Tensor matrix in the (t1, u1, t2, u2) representation, which takes into account crossing symmetry
        n: order of the monomial, or parity under y <-> -y
        """
        return np.array([[1, (-1)**n, 0, 0],
                        [0, 0, 1, (-1)**n]]).transpose()

    class Irrep(Enum):
        SING = 1
        ADJ = 2
        SYM = 3
        ANTI = 4
        def __str__(self):
            return ["Sing", "Adj", "Sym", "Anti"][self.value-1]

    def checkYB(self, S, th1, th2):
        """ Check Yang Baxter equation on S on two points th1, th2 """

        N = self.N
        y1 = yfuntheta(th1)
        y2 = yfuntheta(th2)
        y12 = yfuntheta(th1+th2)

        # S1 = S.eval(y1)
        # S2 = S.eval(y2)
        # S12 = S.eval(y12)
        # eq1 = S1[3]*S12[0]*S2[2] + S1[1]*S12[2]*S2[2] - S1[3]*S12[2]*S2[0]
        # eq2 = S1[0]*S12[3]*S2[2] + S1[2]*S12[3]*S2[0] + S1[2]*S12[1]*S2[2] + self.N*S1[2]*S12[3]*S2[2]
        # print("th={}, {}, eq1:{}".format(th1, th2, eq1))
        # print("th={}, {}, eq2:{}".format(th1, th2, eq2))
        # return abs(eq1) + abs(eq2)

        comps = ("t1","u1","t2","u2")
        S1 = {c:S.eval(y1)[j] for (j,c) in enumerate(comps)}
        S2 = {c:S.eval(y2)[j] for (j,c) in enumerate(comps)}
        S12 = {c:S.eval(y12)[j] for (j,c) in enumerate(comps)}

        S1abs = sum(abs(S.eval(y1)))
        S2abs = sum(abs(S.eval(y2)))
        S12abs = sum(abs(S.eval(y12)))
        Sabstot = S1abs*S2abs*S12abs

        eq1 = S2["t2"]*S12["t2"]*S1["u1"] + S12["t1"]*S2["t2"]*S1["u2"] - S2["t1"]*S12["t2"]*S1["u2"]
        eq2 = S1["t2"]*S2["t2"]*S12["u1"] + S2["t1"]*S1["t2"]*S12["u2"] +\
                S1["t1"]*S2["t2"]*S12["u2"] + N*S1["t2"]*S2["t2"]*S12["u2"]

        # return (abs(eq1) + abs(eq2))/Sabstot

        return abs(eq1), abs(eq2)



class UN2(Group):
    """ Group UN with with constraints t1 = u1 = 0 """

    def __init__(self, N):

        self.parityInv = True

        # Allowed tensor structures
        self.tensors = ["t2", "u2", "r1", "r2"]
        # Allowed independent tensor structures
        self.indepTensors = ["t2", "r1"]

        self.N = N
        # Number of independent components for even functions
        self.leneven = 2
        self.lenodd = 2

        # Crossing symmetry matrix FIXME
        self.d = array([[0, 0, (1+N)/2, (1-N)/2],
                      [0, 0, 1/2, 1/2],
                      [1/N, (-1+N)/N, 0, 0],
                      [-1/N, (1+N)/N, 0, 0]])

        # Matrix to go from (t2, u2, r1, r2) representation to
        # (SING+, SING-, ADJ+, SYM)
        self.toIrrep = array([[N, 0., 1, 1.],
                            [N, 0, -1, -N],
                            [0, 0, 1, 0],
                            [0, 1, 0, 0]])

    def tMat(self, n):
        """ Tensor matrix in the (t2, u2, r1, r2) representation, which takes into account crossing symmetry, and acts on the space of independent tensors (t2, r1)
        n: order of the monomial, or parity under y <-> -y
        """
        return np.array([[1, (-1)**n, 0, 0],
                        [0, 0, 1, (-1)**n]]).transpose()

    class Repr(Enum):
        SINGP = 1
        SINGM = 2
        ADJP = 3
        SYM = 4
        def __str__(self):
            return ["Sing+", "Sing-", "Adj+", "Sym"][self.value-1]


class UN3(Group):
    """ Group UN with with constraints r1 = r2 = 0 and parity violation """

    def __init__(self, N):

        self.parityInv = False

        # Allowed tensor structures
        self.tensors = ["t1L", "u1L", "t2L", "u2L", "t1R", "u1R", "t2R", "u2R"]
        # Allowed independent tensor structures
        self.indepTensors = ["t1+", "t2+", "t1-", "t2-"]

        self.N = N
        # Number of independent components
        self.len = len(self.tensors)

        # Crossing symmetry matrix FIXME
        # XXX MAybe wrong!!!
        self.d = array([[0, 0, (1+N)/2, (1-N)/2],
                      [0, 0, 1/2, 1/2],
                      [1/N, (-1+N)/N, 0, 0],
                      [-1/N, (1+N)/N, 0, 0]])

        # Matrix to go from (t1L, u1L, t2L, u2L, t1R, u1R, t2R, u2R) representation to
        # (SINGL, ADJL, SYML, ANTIL, SINGR, ADJR, SYMR, ANTIR)
        toIrrep = array([[1., 0., N, 0.],
                            [1, 0, 0, 0],
                            [0, 1, 0, 1],
                            [0, 1, 0, -1]])
        self.toIrrep = scipy.linalg.block_diag(toIrrep, toIrrep)
        # print(self.toIrrep.shape)


    def tMat(self, n):
        """ Tensor matrix from self.indepTensors to self.tensors form,  which takes into account crossing symmetry
        n: order of the monomial, or parity under y <-> -y
        """
# ["t1+", "t2+", "t1-", "t2-"]
# self.tensors = ["t1L", "u1L", "t2L", "u2L", "t1R", "u1R", "t2R", "u2R"]

        return np.array([
            [1, (-1)**n, 0, 0, 1, (-1)**n, 0, 0],
            [0, 0, 1, (-1)**n, 0, 0, 1, (-1)**n],
            [1j, 1j*(-1)**n, 0, 0, -1j, -1j*(-1)**n, 0, 0],
            [0, 0, 1j, 1j*(-1)**n, 0, 0, -1j, -1j*(-1)**n],
            ]).transpose()


    class Irrep(Enum):
        SINGL = 1
        ADJL = 2
        SYML = 3
        ANTIL = 4
        SINGR = 5
        ADJR = 6
        SYMR = 7
        ANTIR = 8

        def __str__(self):
            return ["SingL", "AdjL", "SymL", "AntiL", "SingR", "AdjR", "SymR", "AntiR"][self.value-1]

    class Comps(Enum):
        t1L = 1
        u1L = 2
        t2L = 3
        u2L = 4
        t1R = 5
        u1R = 6
        t2R = 7
        u2R = 8

        def __str__(self):
            tensors = ["t1L", "u1L", "t2L", "u2L", "t1R", "u1R", "t2R", "u2R"]
            return tensors[self.value-1]


    # FIXME Only checking two of the 3 equations
    def checkYB(self, S, th1, th2):
        """ Check Yang Baxter equation on S on two points th1, th2 """

        N = self.N

        y1 = yfuntheta(th1)
        y2 = yfuntheta(th2)
        y12 = yfuntheta(th1+th2)

        S1 = {chi:None for chi in ("L","R")}
        S2 = {chi:None for chi in ("L","R")}
        S12 = {chi:None for chi in ("L","R")}


        chis = ("L","R")
        comps = ("t1","u1","t2","u2")
        S1 = {c+chi:S.eval(y1)[4*i+j] for (i,chi) in enumerate(chis) for (j,c) in enumerate(comps)}
        S2 = {c+chi:S.eval(y2)[4*i+j] for (i,chi) in enumerate(chis) for (j,c) in enumerate(comps)}
        S12 = {c+chi:S.eval(y12)[4*i+j] for (i,chi) in enumerate(chis) for (j,c) in enumerate(comps)}

        S1abs = sum(abs(S.eval(y1)))
        S2abs = sum(abs(S.eval(y2)))
        S12abs = sum(abs(S.eval(y12)))
        Sabstot = S1abs*S2abs*S12abs

        # Check left and right equations
        # for chi in chis:
            # t1, u1, t2, u2
            # eq1[chi] = S1[chi][3]*S12[chi][0]*S2[chi][2] + S1[chi][1]*S12[chi][2]*S2[chi][2] - S1[chi][3]*S12[chi][2]*S2[chi][0]

        eq1 = S2["t2L"]*S12["t2L"]*S1["u1R"] + S12["t1L"]*S2["t2L"]*S1["u2R"] - S2["t1L"]*S12["t2L"]*S1["u2R"]
        # XXX Should I always use uL ?
        eq2 = S2["t2R"]*S12["t2R"]*S1["u1L"] + S12["t1R"]*S2["t2R"]*S1["u2L"] - S2["t1R"]*S12["t2R"]*S1["u2L"]

        eq3 = S1["t2L"]*S2["t2R"]*S12["u1L"] + S2["t1R"]*S1["t2L"]*S12["u2L"] +\
                S1["t1L"]*S2["t2R"]*S12["u2L"] + N*S1["t2L"]*S2["t2R"]*S12["u2L"]

        return array([abs(eq1),abs(eq2),abs(eq3)])

