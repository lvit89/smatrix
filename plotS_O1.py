##############################
# Plot parity-invariant S-matrix with one pole, obtained by maximizing coupling
#############################

from o1 import *
import matplotlib.pyplot as plt

nmax = 40
mb= sqrt(3)

gridsize = 30
grid = np.exp(np.arange(gridsize)/gridsize*(pi/2*1j))

bsList = [BoundState(mb)]
# Ansatz for parity-invariant, crossing-symmetric S-matrix
polynom = Polynom(nmax, [True,False,False,False])
# Dimension of vector space
d = len(bsList) + polynom.len

S = Smatrix(bsList, polynom)
obj = np.zeros(d)
# Maximize coupling
obj[0] = -1.

# NOTE Not needed
# Set to 0 odd monomials
# eq_cons = []
# for i in range(1,nmax+1,2):
    # c = np.zeros(d)
    # c[i+1] = 1
    # eq_cons.append(c)

o = Optimizer(grid, S)
res = o.optimize(obj)
gSq = -res['primal objective']

f = array([np.dot(S.linOp(y),res['x']) for y in grid], dtype=complex)
slist = [s(y) for y in grid]
plt.plot(slist, np.real(f), label="Re S")
plt.plot(slist, np.imag(f), label="Im S")
plt.plot(slist, np.absolute(f), label="|S|")
plt.xlabel("s")
plt.title(r'$m_b = \sqrt{3}$')
plt.legend()
plt.savefig("ReImS_mb={}.pdf".format(mb))
