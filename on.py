import numpy as np
from numpy import array
from numpy import log, exp, sqrt, pi, cosh, sinh
from enum import Enum
from cvxopt import solvers, matrix, spmatrix
import cvxopt
from analytic import *
from groups import *


def tail(y,n):
    """ 1/log(s)^n non-analytical contribution at infinity """
    return y**n/(-1+log(1+y**2))


class BoundState():
    """ Class to represent a bound state """
    def __init__(self, mb, N, group, rep):
        self.N = N
        self.mb = mb
        self.group = group
        self.rep = rep

        self.vec = group.boundStateVec(rep)


    def pole(self, y):
        """ Pole function defined in (12) of Vieira et al., excluding the coupling squared"""

        raise RuntimeError("Not yet implemented")

        vec = self.vec
        mb = self.mb
        N = self.N
        d = self.group.d

        J = 1/(2*mb*sqrt(4-mb**2))

        # This is automatically crossing-symmetric
        return -vec*J/(sfuny(y)-mb**2) -np.dot(d,vec)*J/(sfuny(-y)-mb**2)


def vectorize(gSqvec, polynom, ctails=[]):
    """ Representation of the ansatz for the S-matrix as a (3, m) vector, where
    is the size of the Ansatz (number bound states + number monomials in y) """
    # This flattens all the arrays
    return np.concatenate((gSqvec, polynom, array(ctails)), axis=None)


class Smatrix():
    """ Class representing the S-matrix evaluated on the real line """

    def __init__(self, bsList, nmax, group, ntails=0):
        """
        bsList: list of BoundState instances
        N: flavor number
        nmax: maximum degree of polynomials in y
        """
        self.bsList = bsList
        self.N = group.N
        self.nmax = nmax
        self.group = group

        if bsList != []:
            raise RuntimeError("Bound states not yet implemented")


        # Number of independent components
        self.leneven = self.group.leneven
        self.lenodd = self.group.lenodd


        # Total number of polynomial coefficients in the crossing-symmetric Ansatz
        # y-even functions have two independent coefficients, while y-odd functions
        self.lenpol = sum([self.lenodd for i in range(self.nmax+1)  if i%2==1]) +\
                sum([self.leneven for i in range(self.nmax+1) if i%2==0])

        self.ntails = ntails
        # Total number of independent coefficients for the tails
        self.lentails = sum([self.lenodd for i in range(self.ntails)  if i%2==1]) +\
                sum([self.leneven for i in range(self.ntails) if i%2==0])

        # Total size of the ansatz
        self.size = len(self.bsList) + self.lenpol + self.lentails


        # To be set after S-matrix is optimized
        self.param = None


    def evaluate(self, y, gSqvec, polynom, ctails=[]):
        """ Evaluate the (vector) S-matrix at a point given the parameters
        y: point where to evaluate the S-matrix
        gSqvec: numpy array of length n_BS, where n_BS is the number of couplings
        polynom: matrix of polynomial coefficients
        ctails: coefficients in the ansatz
        """

        N = self.N
        nmax = self.nmax
        tMat = self.group.tMat
        toIrrep = self.group.toIrrep

        if len(gSqvec) != len(self.bsList):
            raise RuntimeError("gSqvec of shape incompatible with number of bound states".format(str(gSqvec.shape)))
        if len(polynom) != self.lenpol:
            raise RuntimeError("polynom with shape {} must be of shape {}".format(polynom.shape, self.lenpol))
        if len(ctails) != self.lentails:
            raise RuntimeError("Number of tails coefficients {} must be {}".format(ctails.shape, self.lentails))

        # Add pole contribution
        ret = np.dot(array([bs.pole(y) for bs in self.bsList]).transpose(), gSqvec)

        # Add monomials contribution
        monoms = np.hstack([y**n*tMat(n) for n in range(nmax+1)])
        ret += np.dot(monoms, polynom)

        # Add tails contribution
        tails = np.hstack([tail(y,n)*tMat(n) for n in range(self.ntails)])
        ret += np.dot(tails, array(ctails))

        # Convert to irrep
        return np.dot(toIrrep, ret)

    def checkYB(self, n=100, thmax=5, imax=pi/2):
        """ Return maximal violation of YB equation """
        reth1 = np.random.uniform(-thmax, thmax, n)
        reth2 = np.random.uniform(-thmax, thmax, n)
        imth1 = 1j*np.random.uniform(0, imax, n)
        imth2 = 1j*np.random.uniform(0, imax, n)
        th1List = reth1+imth1
        th2List = reth2+imth2
        yb = [(th1, th2, self.group.checkYB(self, th1, th2)) for (th1,th2) in zip(th1List,th2List)]
        th1 = array([x[0] for x in yb])
        th2 = array([x[1] for x in yb])
        viol = array([x[2] for x in yb])

        return th1, th2, viol
        # return max(yb, key=lambda x:x[2])[2]

    def eval(self, y, irrep=False):
        """ Evaluate S-matrix at y after it has been optimized """

        return np.dot(self.linOp(y, irrep), self.param)


    def linOp(self, y, irrep=True):
        """ Representation of the S-matrix as a linear operator. Returns a vector with shape (len, m), where m is the
        number of input parameters, and len is the number of independent parameters
        irrep: Convert to irrep form from (t, u, l) form
        """

        N = self.N
        nmax = self.nmax
        tMat = self.group.tMat
        toIrrep = self.group.toIrrep

        if len(self.bsList)==0:
            poles = array([[]]*len(self.group.Irrep))
        else:
            poles = array([bs.pole(y) for bs in self.bsList], dtype=complex).transpose()


        monoms = np.hstack([y**n*tMat(n) for n in range(nmax+1)])

        if self.lentails == 0:
            tails = array([[]]*len(self.group.Irrep))
        else:
            tails = np.hstack([tail(y,n)*tMat(n) for n in range(self.ntails)])

        if irrep:
            # print(poles.shape)
            # print(monoms.shape)
            # print(tails.shape)
            # m = np.hstack((poles, monoms, tails))
# XXX Reintroduce poles ?
            m = np.hstack((monoms, tails))
            # print(m.shape)
            return np.dot(toIrrep, m)
        else:
            return np.hstack((poles, monoms, tails))


class Optimizer():
    """ http://cvxopt.org/userguide/coneprog.html#second-order-cone-programming """

    def __init__(self, grid, S, quiet=False):
        """
        grid: grid of point on the physical positive line where the unitarity constraints will be enforced
        S: Smatrix instance
        """
        self.grid = grid
        self.S = S

        if quiet:
            solvers.options['show_progress'] = False


    def optimize(self, obj, cons=[]):
        """ Minimize a linear functional of the S-matrix, satisfying the constrains |S(s_i)|^2<=1
        obj: linear functional to maximize
        cons: linear equality constraints
        """

        group = self.S.group
        grid = self.grid
        nmax = self.S.nmax
        bsList = self.S.bsList
        N = self.S.N
        # Number of independent representations on which we impose quadratic constraints
        Nrep = len(group.Irrep)

        # Dimension of the vector space
        D = self.S.size

        # Number of quadratic inequality constraints
        n_ineq_cons = Nrep*len(self.grid)

        if len(obj) != D:
            raise RuntimeError("Objective linear function should have dimension {}".format(D))

        # Representation of the S-matrix as a linear operator for each point on the grid, in the irrep representation
        ops = {y: self.S.linOp(y) for y in self.grid}

        c = matrix(obj)
        G = [matrix(array([np.zeros(D), np.real(ops[y][i]), np.imag(ops[y][i])])) for y in grid for i in range(Nrep)]
        h = [matrix([1.,0.,0.])]*n_ineq_cons

        Nbs = len(bsList)
        # Impose positivity of g^2
        G0 = spmatrix(-1., I=np.arange(Nbs), J=np.arange(Nbs), size=(Nbs, D))
        h0 = matrix(np.zeros(Nbs))

        # Add custom equality constraints
        if cons!=[]:
            A = matrix(np.vstack(cons))
            b = matrix(np.zeros(len(cons)))
        else:
            A = None
            b = None

        ret = solvers.socp(c=c, Gq=G, hq=h, Gl=G0, hl=h0, A=A, b=b)
        self.S.param = np.array(ret['x']).flatten()

        return ret
