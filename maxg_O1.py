#############################
# Plot of the maximum coupling to bound state for parity-invariant theory with one bound state
############################

from o1 import *
import matplotlib.pyplot as plt
from sys import argv, exit
from matplotlib import rc

form = "png"

quiet = False
structures = [True,False,False,False]
# structures = [True,True,False,False]
structures = [True,True,True,True]

parity = False

struct = "".join(map(lambda x: str(int(x)),structures))

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# params = {'legend.fontsize': 8, 'lines.markersize':2.5, 'lines.marker':"o"}
# plt.rcParams.update(params)
plt.style.use('ggplot')

if len(argv)<2:
    print("{} <nmax>".format(argv[0]))
    exit(1)

nmax = int(argv[1])
mbSqList = np.linspace(0.01, 3.99, 50)

# Ansatz for parity-invariant, crossing-symmetric S-matrix
polynom = Polynom(nmax, structures)
# Dimension of vector space
d = 1 + polynom.len

gridfactor = 4
gridsize = gridfactor*nmax
grid = np.exp(2*pi*1j*np.arange(gridsize)/gridsize)

maxLogGsq = []

for mb in sqrt(mbSqList):
    bsList = [BoundState(mb, structures, parity)]

    S = Smatrix(polynom, bsList)
    obj = np.zeros(d)
    # Maximize coupling
    obj[0] = -1.

    # NOTE constraints not needed
    # Set to 0 odd monomials
    # eq_cons = []
    # for i in range(1,nmax+1,2):
        # c = np.zeros(d)
        # c[i+1] = 1
        # eq_cons.append(c)

    o = Optimizer(grid, S, quiet=quiet)
    res = o.optimize(obj)
    gSq = -res['primal objective']
    maxLogGsq += [log(gSq)]

plt.plot(mbSqList, maxLogGsq)
plt.title("nmax={}, struct={}".format(nmax, struct))
plt.xlabel("g")
plt.ylabel(r"max $\log g^2$")
plt.savefig("maxLogGsq_struct={}.{}".format(struct, form))
