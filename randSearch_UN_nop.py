from on import *
from analytic import *
import sys
import database

Break = False
groupClass = UN3

saveondb = True
if saveondb:
    db = database.Database()

quiet = True
ntails = 2

samples = 1000
thetamax = 10
# Number of points on the unitarity grid over number of states
gridfactor = 4

argv = sys.argv
if len(argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)


N = int(sys.argv[1])
nmax = int(sys.argv[2])

group = groupClass(N)
d = group.d
Repr = group.Irrep

bsList = []

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

S = Smatrix(bsList, nmax, group, ntails)

# NOTE Fix to zero the imaginary part of S_sing at the crossing symmetric point
# to get rid of the global phase
eq_cons = [np.imag(S.linOp(y=0)[0])]

o = Optimizer(grid, S, quiet=True)

# List of real theta where to check unitarity
thlist = np.linspace(0,thetamax,1000)
# Corresponding list of y
ylist = yfunthetaRe(thlist)
ops = [S.linOp(y) for y in ylist]

nzerosVec = np.zeros(samples)
satVec = np.zeros(samples)

for i in range(samples):
    # Maximize random functional of the S-matrix at y=0
    coeff = np.random.normal(size=len(Repr))

    # NOTE Get rid of the global phase parameter by setting to zero
    # the parity-violating part of the singlet component at the crossing symmetric point
    coeff[4] = coeff[0]

    # Extract a real functional by multiplying by random phases
    phases = np.random.uniform(0, pi, size=len(Repr))

    # NOTE Consider only the real part of the singlet S-matrix at the crossing
    # symmetric point
    phases[0] = 0
    phases[4] = 0

    obj = np.real(np.einsum("i,i,ij -> j", coeff, exp(1j*phases), S.linOp(y=0)))

    res = o.optimize(obj, eq_cons)
    # print(res['primal objective'])

    f = array([np.dot(op, res['x']) for op in ops], dtype=complex).squeeze().transpose()

    nzeros = np.sum(abs(np.imag(f)-np.roll(np.imag(f),1,axis=1))[:,1:], axis=1)

    # We want S-matrix to be non-trivial both in left and right sectors
    nzerosL = np.sum(nzeros[:4])
    nzerosR = np.sum(nzeros[4:])

    nzerosVec[i] = nzerosL + nzerosR

    sat = np.mean(np.abs(f))
    satVec[i] = sat

    # if sat>0.98 and nzeros>0.98 and nzeros<1.02:
    if sat>0.99 and nzerosL>0.2 and nzerosR>0.2:
        print("sat:{}, nzerosL:{}, nzerosR:{}".format(sat, nzerosL, nzerosR))

        if saveondb:
            data = dict(N=N, nmax=nmax, ntails=ntails, gridfactor=gridfactor, obj=obj, coeff=coeff, param=S.param)
            data["phases"] = phases

            db.insert(data)

        if Break:
            sys.exit(1)


plt.scatter(nzerosVec, satVec)
plt.xlabel("nzeros")
plt.ylabel(r"$\bar{|S|}$")
plt.savefig("nzeros_hist.pdf")
