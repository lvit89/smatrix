from o1 import *
import matplotlib.pyplot as plt
from analytic import *
import sys
import database

Break = False
saveondb = True
quiet = True

sigma = 1/2

# Ansatz for parity-violating S-matrix without anti-particles
structures = [1, 0, 0, 1]

thetamax = 10
nmax = 100
gridfactor = 4

if saveondb:
    db = database.Database(tablename='HSG')

gridsize = gridfactor*nmax
thetagrid = np.linspace(-thetamax, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

polynom = Polynom(nmax, [True,False,False,True])
S = Smatrix(polynom)

# Impose zero at y=y0 (the imaginary part is zero by construction)
# eq_cons = [np.real(polynom.eval(y0))]
eq_cons = []

o = Optimizer(grid, S, quiet=True)

# List of real theta where to check unitarity
thlist = np.linspace(0,thetamax,1000)
# Corresponding list of y
ylist = yfunthetaRe(thlist)
ops = [S.linOp(y) for y in ylist]

samples = 10
nzerosVec = np.zeros(samples)
satVec = np.zeros(samples)


for i in range(samples):

    # Maximize random functional of the S-matrix
    d = polynom.len
    obj = np.zeros(d)

    # Maximize random combination of leading coefficients in parity even and parity-odd sectors
    coeff = np.random.normal(size=2)
    obj[0] = coeff[0]
    obj[len(polynom.powersEven)] = coeff[1]

    res = o.optimize(obj, eq_cons)
    # print(res['primal objective'])

    f = array([np.dot(op, res['x']) for op in ops], dtype=complex)
    imagf = np.imag(f).transpose()[0]

    nzeros = sum(abs(imagf-np.roll(imagf,1)))/2
    # if nzeros<1.9 and nzeros>0.1:
        # print("nzeros={}, obj={}".format(nzeros, obj[:2]))

    nzerosVec[i] = nzeros

    sat = np.mean(np.abs(f))
    satVec[i] = sat

    # if sat>0.98 and nzeros>0.98 and nzeros<1.02:
    if sat>0.9 and nzeros>0.2:
        print("sat:{}, coeff=[{}]".format(sat, ",".join(map(str,coeff))))

        # print(np.array(structures).dtype)

        if saveondb:
            data = dict(nmax=nmax, gridfactor=gridfactor, obj=obj, coeff=coeff, param=S.param,
                    # Need to convert this
                    structures=np.array(structures).astype(float))
            db.insert(data)

        if Break:
            sys.exit(1)


plt.scatter(nzerosVec, satVec)
plt.xlabel("nzeros")
plt.ylabel(r"$\bar{|S|}$")
plt.savefig("nzeros_hist.pdf")
