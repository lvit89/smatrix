from o1 import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit
import database

table = "HSG"

form = "png"
thetamax = 10


rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
plt.style.use('ggplot')

db = database.Database(tablename=table)

for row in db.get():
    param = row['param']
    nmax = row['nmax']
    structures = row['structures'].astype(int)
    Id = row['id']

    polynom = Polynom(nmax, structures)
    S = Smatrix(polynom)
    S.param = param
    print(param)

#############################
# Plot at physical energy
############################

    thlist = np.linspace(-thetamax, thetamax, 100)
    ylist = yfunthetaRe(thlist)

    fig, ax  = plt.subplots(2,1,sharex=True)
    fig.set_figheight(8)
    fig.set_figwidth(5)

    f = [array([np.dot(S.linOp(y,chi), S.param) for y in ylist], dtype=complex).flatten() for chi in ('L','R')]

    for i,chi in enumerate(('L','R')):
        ax[i].plot(thlist, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(chi))
        ax[i].plot(thlist, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(chi))
        ax[i].plot(thlist, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(chi))

        ax[i].legend(loc=4, prop={'size':6})


    fig.suptitle(r"table={}, id={}".format(table, Id))
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.xlabel(r"$\theta$")
    plt.savefig("plots/table={}_id={}_ReTh.{}".format(table, Id, form))
    plt.clf()
