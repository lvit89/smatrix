import numpy as np
from scipy.special import gamma
from numpy import array, pi, exp, log, sqrt, tanh, cosh, sinh, cos, tan, arctan, arccosh

def yfunthetaRe(theta):
    """ Value of y corresponding to positive real theta with small imaginary part """
    return 1/cosh(theta) +1j*tanh(theta)

def thetafuns(s):
    """ theta as function of s outside the cuts """
    return log(1/2*(-2+1j*sqrt(s*(4-s))+s))

def sfuntheta(th):
    return 4*cosh(th/2)**2

def yfuntheta(th):
    return yfuns(sfuntheta(th))

def yfuns(s):
    """ Variable y as function of s """
    return (2-sqrt(s*(4-s)))/(s-2)

def sfuny(y):
    """ Mandelstam variable s as function of y """
    return 2*(1+y)**2/(1+y**2)

def massGN(N):
    """ Bound state masses of O(N) Gross-Neveu models """
    return 2*cos(pi/(N-2))

def lambdaGN(N):
    return 2*pi/(N-2)

def qGN(th, lam):
    th = th/(2*pi)
    lam = lam/(2*pi)
    return gamma(-lam-1j*th)*gamma(1/2-1j*th)/(gamma(1/2-lam-1j*th)*gamma(-1j*th))

def SGN(th, N):
    """ Analytic Gross-Neveu matrix """
    lam = lambdaGN(N)
    S2 = qGN(th,lam)*qGN(1j*pi-th,lam)
    S1 = -1j*lam/(1j*pi-th)*S2
    S3 = -1j*lam/(th)*S2

    return array([N*S1+S2+S3, S2-S3, S2+S3])

def lambdaON(N):
    return 2*pi/(N-2)

def qON(th, lam):
    th = th/(2*pi)
    lam = lam/(2*pi)
    return gamma(lam-1j*th)*gamma(1/2-1j*th)/(gamma(1/2+lam-1j*th)*gamma(-1j*th))

def SON(th, N):
    """ Analytic O(N) sigma-model matrix """
    lam = lambdaON(N)
    S2 = qON(th,lam)*qON(1j*pi-th,lam)
    S1 = -1j*lam/(1j*pi-th)*S2
    S3 = -1j*lam/(th)*S2

    return array([N*S1+S2+S3, S2-S3, S2+S3])

def F(th, x):
    return gamma((x+1j*th)/(2*pi))*gamma((x-1j*th+pi)/(2*pi))/(gamma((x-1j*th)/(2*pi))*gamma((x+1j*th+pi)/(2*pi)))


def SONII(th, N, kmax):
    """ Class II O(N) S-matrix """
    nu = arccosh(N/2)
    c = np.prod([F(-th, pi+1j*pi**2*k/nu) for k in range(-kmax,kmax+1)])
    return c*array(
            [sinh(nu*(1-1j*th/pi))/sinh(nu*(1+1j*th/pi)),
                -1,
                1])

def thetapGN(N):
    """ Location of pole in GN theories """
    return 2*pi/(n-2)

def f(thh, lam):
    return gamma(1/2+thh/2)*gamma(1/2+lam/2-thh/2)/(gamma(1/2-thh/2)*gamma(1/2+lam/2+thh/2))

def SUN_GN(th, N):
    """ Analytic U(N) Gross-Neveu chiral S-matrix (not minimal) """
    lam = 2/N
    thh = th/(1j*pi)
    t1 = f(thh, -lam)
    t2 = -lam/(1-thh)*t1
    u1 = f(1-thh, -lam)
    u2 = -lam/thh*u1

    return array([t1+N*t2, t1, u1+u2, u1-u2])



def SUN(th, N):
    """ Analytic U(N) Gross-Neveu S-matrix (minimal) """
    lam = 2/N
    thh = th/(1j*pi)
    t1 = f(thh, lam)
    t2 = -lam/(1-thh)*t1
    u1 = f(1-thh, lam)
    u2 = -lam/thh*u1

    return array([t1+N*t2, t1, u1+u2, u1-u2])


def HSGk2(th, sigma, chi):
    """ Homogeneous SU(3), k=2 Sine-Gordon S-matrix """
    if chi=='L':
        sgn = 1
    elif chi=='R':
        sgn = -1
    return (1-1j*sgn*exp(sigma-sgn*th))/(1+sgn*1j*exp(sigma-sgn*th))


def HSG(th, sigma, k):
    """ Homogeneous SU(3), Sine-Gordon S-matrix """
    return exp(1j*pi/k)*sinh(1/2*(-1j*pi/k+th-sigma))/sinh(1/2*(1j*pi/k+th-sigma))
