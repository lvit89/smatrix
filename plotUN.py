from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit

# Plot minimal class II U(N) models

quiet = False
ntails = 2
thetamax = 10

# Number of points on the unitarity grid over number of states
gridfactor = 3

if len(sys.argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)


N = int(sys.argv[1])
nmax = int(sys.argv[2])

group = UN(N)
d = group.d
Repr = group.Irrep

# Location of zero in theta strip for symmetric component (it is inside strip only for N >= 5)
theta0 = 1j*2*pi/N
# Location of zero in y plane
y0 = yfuntheta(theta0)

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# params = {'legend.fontsize': 8, 'lines.markersize':2.5, 'lines.marker':"o"}
# plt.rcParams.update(params)
plt.style.use('ggplot')

reps = []
bsList = []

S = Smatrix(bsList, nmax, group, ntails)

# obj = S.linOp(y=0)[0]-S.linOp(y=0)[2]
obj = -S.linOp(y=0)[1]

cons = []
# Only impose zeros in symmetric channel, at theta=i*lambda
for i in (2,):
    x = S.linOp(y=y0)[i]
    # Only real part does not vanish
    cons.append(np.real(x))


o = Optimizer(grid, S, quiet=quiet)
res = o.optimize(obj, cons=cons)
print("Maximum obj:", res['primal objective'])

# Check that S-matrix is zero where requested
np.testing.assert_almost_equal(np.dot(S.linOp(y0), res['x'])[2], 0.)

print("Max YB violation: ", np.max(S.checkYB(n=200)[2],axis=0))


#############################
# Plot at physical energy
############################

thetaList = np.linspace(0.01, thetamax, 100)
ylist = yfunthetaRe(thetaList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(op, res['x']) for op in ops], dtype=complex)).transpose()
fAnalytic = array([SUN(th, N) for th in thetaList]).transpose()

# "Unitarity saturation"
# sat = np.absolute(f).flatten().mean()
# print("sat: ", sat)


# Check that solution is crossin symmetric
for y in ylist:
    S1 = np.dot(S.linOp(y), res['x'])
    S2 = np.dot(d, np.dot(S.linOp(-y),res['x']))
    np.testing.assert_almost_equal(S1, S2)

# Check that analytic solution is crossing symmetric
for th in thetaList:
    f1 = SUN(th, N)
    f2 = np.dot(d, SUN(1j*pi-th, N))
    np.testing.assert_almost_equal(f1, f2)

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

for i, rep in enumerate(list(Repr)):
    ax[i].plot(thetaList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    ax[i].plot(thetaList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(fAnalytic[i]), label=r"$|S|^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=4, prop={'size':6})

fig.suptitle(r"$U(N)$ S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}".format(N, nmax, gridsize))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("UN_N={}.pdf".format(N))
plt.clf()


#############################
# Plot at imaginary theta
############################

# Location of pole
# thetap = np.arccosh(cos(2*pi/(N-2)))
# yp = imThtoY(np.imag(thetap))

imthList = np.linspace(0.01, pi-0.01, 100)
ylist = yfuntheta(1j*imthList)
ops = [S.linOp(y) for y in ylist]


f = np.squeeze(array([np.dot(S.linOp(y), res['x']) for y in ylist], dtype=complex)).transpose()
fAnalytic = array([SUN(th, N) for th in 1j*imthList]).transpose()

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

# Legend positions
loc = [1,1,4,1]


for i, rep in enumerate(list(Repr)):
    ax[i].plot(imthList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
    # ax[i].plot(imthList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))

    # Plot analytic prediction
    ax[i].plot(imthList, np.real(fAnalytic[i]), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))
    # ax[i].plot(imthList, np.imag(fAnalytic[i]), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=loc[i], prop={'size':6})
    ax[i].set_ylabel(r"$S_{{\rm {}}}$".format(str(rep)))

fig.suptitle(r"$U(N)$ S-matrix, N={}, $n_{{\rm max}}$={}, gridsize={}".format(N, nmax, gridsize))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$Im \theta$")
# plt.title(r'$m_b = \sqrt{3}$')
plt.savefig("UN_N={}_imTheta.pdf".format(N))
