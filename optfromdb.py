from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit
import database

plot = True
quiet = True

# threshold for YB violation below which we don't plot the functions
thres = 10**-4
# thres = 1

# Maximum imaginary value of theta where to check Yang Baxter
imax = pi/2

# dbname = "data/coeffRemote.db"
dbname = "data/coeff.db"
groupClass = UN3
table = "UN3"

# Plot the irreducible representations instead of (t1, u1, t2, u2)
show = "Irrep"
# Plot (t1, u1, t2, u2)
# show = "Comps"

form = "pdf"
thetamax = 10

if len(argv) < 2:
    print("{} <nmax> [<id>]".format(argv[0]))
    exit(1)

nmax = int(argv[1])

IId = None
try:
    IId = int(argv[2])
except IndexError:
    pass


gridfactor = 4
ntails = 2

# Number of points on the unitarity grid over number of states
gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

# rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
plt.style.use('ggplot')

db = database.Database(dbname=dbname, tablename=table)


for row in db.get():
    if IId!=None and row['id']!=IId:
        continue

    N = row['N']
    Id = row['id']

    group = groupClass(N)
    d = group.d

    if show=="Irrep":
        Repr = group.Irrep
    else:
        Repr = group.Comps

    S = Smatrix(bsList=[], nmax=nmax, group=group, ntails=ntails)

    # S.param = row['param']
    # if S.checkYB(imax=imax) > thres:
        # continue
    # S.param = None

    o = Optimizer(grid, S, quiet=quiet)

    coeff = row['coeff']
    phases = row['phases']

    # FIXME It should already be an angle
    # phases = np.einsum("ij,j ->i", phases.reshape(8,2), [1,1j])
    # obj = np.real(np.einsum("i,i,ij -> j", coeff, phases, S.linOp(y=0)))

    obj = np.real(np.einsum("i,i,ij -> j", coeff, exp(1j*phases), S.linOp(y=0)))

    eq_cons = []
    res = o.optimize(obj, eq_cons)
    param = S.param

    viol = S.checkYB(imax=imax, thmax=5)[2]

    maxYB = np.max(viol, axis=0)
    meanYB = np.mean(viol, axis=0)
    stdYB = np.std(viol, axis=0)
    print("id={}, max:{}".format(Id, maxYB))
    print("mean: ", meanYB)
    print("std: ", stdYB)

    # if not plot or maxYBviol>thres:
    if not plot:
        continue
    print("Plotting...")

#############################
# Plot at physical energy
############################

    thetaList = np.linspace(-thetamax, thetamax, 100)
    ylist = yfunthetaRe(thetaList)
    ops = [S.linOp(y) for y in ylist]

    f = np.squeeze(array([np.dot(op, param) for op in ops], dtype=complex)).transpose()

    h = int(len(Repr)/2)
    fig, ax  = plt.subplots(h,2,sharex=True)
    fig.set_figheight(8)
    fig.set_figwidth(5)

    for i, rep in enumerate(list(Repr)):
        idx = (i%h, int(i/h))
        ax[idx].plot(thetaList, -np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
        ax[idx].plot(thetaList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
        ax[idx].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

        ax[idx].legend(loc=4, prop={'size':6})

    fig.suptitle(r"id={}, nmax={}, ntails={}, max YB viol={}".format(Id, nmax, ntails,  maxYB))
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.xlabel(r"$\theta$")
    plt.savefig("plots/id={}_nmax={}_opt_ReTh.{}".format(Id, nmax, form))
    plt.clf()

#############################
# Plot at imaginary theta
############################

    imthList = np.linspace(0.01, pi-0.01, 100)
    ylist = yfuntheta(1j*imthList)
    ops = [S.linOp(y) for y in ylist]

    f = np.squeeze(array([np.dot(S.linOp(y), param) for y in ylist], dtype=complex)).transpose()

    h = int(len(Repr)/2)
    fig, ax  = plt.subplots(h,1,sharex=True)
    fig.set_figheight(8)
    fig.set_figwidth(5)

    for i,rep in enumerate(list(Repr)):

# Only plot left component
        if i >=4:
            break

        ax[i].plot(imthList, -np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
        ax[i].plot(imthList, -np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))


        ax[i].legend(prop={'size':6})
        ax[i].set_ylabel(r"$S_{{\rm {}}}$".format(str(rep)))

    fig.suptitle(r"id={}".format(Id))
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.xlabel(r"$Im \theta$")

    plt.savefig("plots/id={}_nmax={}_opt_ImTh.{}".format(Id, nmax, form))
    plt.close()
