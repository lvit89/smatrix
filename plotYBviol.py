from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit
import database

plot = True
quiet = True

# Maximum imaginary value of theta where to check Yang Baxter
imax = 0.1

dbname = "data/coeffRemote.db"
# dbname = "data/coeff.db"
groupClass = UN3
table = "UN3"

# Plot the irreducible representations instead of (t1, u1, t2, u2)
show = "Irrep"
# Plot (t1, u1, t2, u2)
# show = "Comps"

form = "png"
thetamax = 10

if len(argv) < 2:
    print("{} <nmax> [<id>]".format(argv[0]))
    exit(1)

nmax = int(argv[1])

IId = None
try:
    IId = int(argv[2])
except IndexError:
    pass


gridfactor = 4
ntails = 2

# Number of points on the unitarity grid over number of states
gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

# rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
plt.style.use('ggplot')

db = database.Database(dbname=dbname, tablename=table)

for row in db.get():
    if IId!=None and row['id']!=IId:
        continue

    N = row['N']
    Id = row['id']

    group = groupClass(N)
    d = group.d

    if show=="Irrep":
        Repr = group.Irrep
    else:
        Repr = group.Comps

    S = Smatrix(bsList=[], nmax=nmax, group=group, ntails=ntails)


    coeff = row['coeff']
    phases = row['phases']
    obj = np.real(np.einsum("i,i,ij -> j", coeff, exp(1j*phases), S.linOp(y=0)))

    eq_cons = []
    o = Optimizer(grid, S, quiet=quiet)
    res = o.optimize(obj, eq_cons)
    param = S.param


    th1, th2, viol = S.checkYB(imax=imax)
    print(viol.mean())
    print(max(viol))
