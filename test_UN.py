from on import *
import matplotlib.pyplot as plt
import pytest
import random
from groups import *

# @pytest.mark.skip(reason="Not needed now")
def test_S():
    """ Test that two ways to compute the S-matrix are equivalent """

    ntails = 2

    # Flavor number
    N = 10
    group = UN(N)

    # Number of bound states
    NBS = 0
    mlist = np.random.rand(NBS)
    reprList = np.random.choice(list(group.Repr), NBS)
    # bsList = array([BoundState(mlist[i], N, group, reprList[i]) for i in range(NBS)])
    bsList = []
    gSqvec = np.random.uniform(sqrt(2)+0.01, 1.99, NBS)

    nmax = 30
    S = Smatrix(bsList, nmax, group, ntails)

    ctails = np.random.rand(S.lentails)

    polynom = np.random.rand(S.lenpol)

    # Sample in the unit disk
    size = 100
    # size = 1
    r = np.random.uniform(0,1, size)
    theta = np.random.uniform(0, 2*pi, size)
    ylist = r*np.exp(theta*1j)

    for y in ylist:
        S1 = S.evaluate(y, gSqvec, polynom, ctails)
        op = S.linOp(y)
        v = vectorize(gSqvec, polynom, ctails)

        S2 = np.dot(op, v)

        np.testing.assert_almost_equal(S1, S2)


# @pytest.mark.skip(reason="Not needed now")
def test_symmetric():
    """ Test that the S-matrix is crossing symmetric """

    # Flavor number
    N = 10
    group = UN(N)
    d = group.d

    # Number of bound states
    NBS = 0
    mlist = np.random.rand(NBS)
    reprList = np.random.choice(list(group.Repr), NBS)
    # bsList = array([BoundState(mlist[i], N, group, reprList[i]) for i in range(NBS)])
    bsList = []
    gSqvec = np.random.uniform(sqrt(2)+0.01, 1.99, NBS)

    ntails = 10
    nmax = 30

    S = Smatrix(bsList, nmax, group, ntails)

    ctails = np.random.rand(S.lentails)
    polynom = np.random.rand(S.lenpol)

    # Sample in the unit disk
    r = np.random.uniform(0,1, 100)
    theta = np.random.uniform(0, 2*pi, 100)
    ylist = r*np.exp(theta*1j)

    for y in ylist:
        S1 = S.evaluate(y, gSqvec, polynom, ctails)
        S2 = np.dot(d, S.evaluate(-y, gSqvec, polynom, ctails))
        np.testing.assert_almost_equal(S1, S2)

# @pytest.mark.skip(reason="Not needed now")
def test_opt_free():
    """ Test that maximizing the S-matrix in the symmetric channel without bound states leads to free S-matrix """


    ntails = 2
    nmax = 50
    gridsize = 2*nmax

    # thetamax = 10.
    # thetagrid = np.linspace(0, thetamax, gridsize)
    # grid = yfunthetaRe(thetagrid)

    grid = np.exp(np.arange(gridsize)/gridsize*(pi/2*1j))

    bsList = []

    Nlist = np.random.choice(range(2,20), 3)

    for N in Nlist:

        group = UN(N)

        S = Smatrix(bsList, nmax, group, ntails)
        # Hilbert space dimension
        # obj = np.zeros(S.size)

        # Maximize symmetric of S-matrix at crossing symmetric point
        obj = -S.linOp(y=0.)[2]

        o = Optimizer(grid, S)
        res1 = array(o.optimize(obj)['x']).flatten()

        print(res1)

        # Analytical solution
        res2 = np.zeros(S.size)
        res2[0] = 1.

        np.testing.assert_allclose(res1, res2, atol=10**-3)
