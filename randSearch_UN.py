from on import *
import matplotlib.pyplot as plt
from analytic import *
import sys
import database

Break = False
groupClass = UN3

saveondb = True
if saveondb:
    db = database.Database()

quiet = True
ntails = 2

samples = 1000
thetamax = 10
# Number of points on the unitarity grid over number of states
gridfactor = 4

argv = sys.argv
if len(argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)


N = int(sys.argv[1])
nmax = int(sys.argv[2])

group = groupClass(N)
d = group.d
Repr = group.Repr

bsList = []

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

S = Smatrix(bsList, nmax, group, ntails)

# Impose zero at y=y0 (the imaginary part is zero by construction)
# eq_cons = [np.real(polynom.eval(y0))]
eq_cons = []

o = Optimizer(grid, S, quiet=True)

# List of real theta where to check unitarity
thlist = np.linspace(0,thetamax,1000)
# Corresponding list of y
ylist = yfunthetaRe(thlist)
ops = [S.linOp(y) for y in ylist]

nzerosVec = np.zeros(samples)
satVec = np.zeros(samples)

for i in range(samples):
    # Maximize random functional of the S-matrix at y=0
    coeff = np.random.normal(size=len(Repr))

    obj = np.einsum("i,ij -> j", coeff, S.linOp(y=0))

    res = o.optimize(obj, eq_cons)
    # print(res['primal objective'])

    f = array([np.dot(op, res['x']) for op in ops], dtype=complex)

    imagf = np.imag(f).transpose()[0,0]

    nzeros = sum(abs(imagf-np.roll(imagf,1)))/2
    nzerosVec[i] = nzeros

    sat = np.mean(np.abs(f))
    satVec[i] = sat

    # if sat>0.98 and nzeros>0.98 and nzeros<1.02:
    if sat>0.99 and nzeros>0.2:
        print("sat:{}, nzeros:{}".format(sat, nzeros))

        if saveondb:
            data = dict(N=N, nmax=nmax, ntails=ntails, gridfactor=gridfactor, obj=obj, coeff=coeff, param=S.param)
            db.insert(data)

        if Break:
            sys.exit(1)


plt.scatter(nzerosVec, satVec)
plt.xlabel("nzeros")
plt.ylabel(r"$\bar{|S|}$")
plt.savefig("nzeros_hist.pdf")
