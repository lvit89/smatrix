##############################
# Homogenous Sine-Gordon theory for SU(3), k=2 (parity-violating, no anti-particles)
#############################

from o1 import *
import matplotlib.pyplot as plt
from numpy import tanh
from sys import argv, exit

if len(argv) < 2:
    print("{} <sigma>".format(argv[0]))
    exit(1)

sigma = float(argv[1])


form = "png"
quiet = False

# Ansatz for parity-violating S-matrix without anti-particles
structures = [1, 0, 0, 1]

nmax = 400

# Location of resonance in the y plane, given that in the theta plane it's at  sigma + i pi/2
y0 = 1j*tanh(sigma/2)
print("y0: {}".format(y0))

thetamax = 10
gridfactor = 4
gridsize = gridfactor*nmax
# Unitarity is also imposed at negative theta
thgrid = np.linspace(-thetamax, thetamax, gridsize)
grid = yfunthetaRe(thgrid)

polynom = Polynom(nmax, structures)
# Dimension of vector space
d = polynom.len

S = Smatrix(polynom)
obj = np.zeros(d)
# Maximize S-matrix at crossing symmetric point
# obj[0:2] = [0.3526668,  0.18649888]
obj[0] = sigma
obj[len(polynom.powersEven)] = 1.


# Impose zero at y=y0 (the imaginary part is zero by construction)
eq_cons = []
# eq_cons = [np.real(polynom.eval(y0))]

o = Optimizer(grid, S, quiet=quiet)
res = o.optimize(obj, eq_cons)

print(np.array(res['x']).flatten())

# List of real theta where to plot S-matrix
thlist = np.linspace(-5,5,100)
# Corresponding list of y, when the imaginary part of theta is small from above
ylist = yfunthetaRe(thlist)

fig, ax  = plt.subplots(2,1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

f = [array([np.dot(S.linOp(y,chi),res['x']) for y in ylist], dtype=complex).flatten() for chi in ('L','R')]

for i,chi in enumerate(('L','R')):

    ax[i].plot(thlist, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(chi))
    ax[i].plot(thlist, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(chi))
    ax[i].plot(thlist, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(chi))

    fth = HSG(thlist, sigma, chi)
    ax[i].plot(thlist, np.real(fth), label=r"$Re S^{{\rm th}}_{{\rm {}}}$".format(chi), linestyle='--', linewidth=2)
    ax[i].plot(thlist, np.imag(fth), label=r"$Im S^{{\rm th}}_{{\rm {}}}$".format(chi), linestyle='--', linewidth=2)

    ax[i].legend(loc=4, prop={'size':6})

plt.suptitle(r'HSG, $SU(3)_2$, $\sigma=${}'.format(sigma))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")

plt.savefig("ReImHSG_k=2_sigma={}.{}".format(sigma, form))
