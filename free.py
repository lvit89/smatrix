from on import *
import matplotlib.pyplot as plt
import pytest
import random

N = 10

group = UN(N)
d = group.d
Repr = group.Repr
ntails = 2

nmax = 100
gridsize = 2*nmax
# grid = np.exp(np.arange(gridsize)/gridsize*(pi/2*1j))
grid = exp(1j*np.linspace(0.,pi/2,gridsize))

bsList = []

S = Smatrix(bsList, nmax, group, ntails)
# Hilbert space dimension
D = S.size

# obj = S.linOp(y=0)[0]+S.linOp(y=0)[1]+S.linOp(y=0)[2]
# print(obj)

# Maximize symmetric component
obj = S.linOp(y=0)[2]

o = Optimizer(grid, S)
res = array(o.optimize(obj)['x']).flatten()


thetaList = np.linspace(0, 10, 100)
ylist = yfunthetaRe(thetaList)
ops = [S.linOp(y) for y in ylist]

f = np.squeeze(array([np.dot(op, res) for op in ops], dtype=complex)).transpose()

fig, ax  = plt.subplots(len(Repr),1,sharex=True)
fig.set_figheight(8)
fig.set_figwidth(5)

for i, rep in enumerate(list(Repr)):
    ax[i].plot(thetaList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
    ax[i].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

    ax[i].legend(loc=4, prop={'size':6})
    ax[i].set_ylim(-1.1,1.1)

plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")
plt.savefig("test.pdf".format(N))
