import numpy as np
from numpy import array
from numpy import log, exp, sqrt, pi, tanh, cosh
from enum import Enum
from cvxopt import solvers, matrix, spmatrix
import cvxopt
from analytic import *


class BoundState():
    """ Class to represent a bound state """
    def __init__(self, mb, structures, parity=True):
        self.mb = mb
        self.structures = structures
        self.parity = parity

    def pole(self, y, chi):
        """ Pole function defined in (3) of Penedones et al., excluding the coupling squared"""
        mb = self.mb
        J = 1/(2*mb*sqrt(4-mb**2))

        ret1 = -J/(sfuny(y)-mb**2)
        ret2 = -J/(4-sfuny(y)-mb**2)

        if self.parity:
            if self.structures[1]:
                return ret1
            else:
                return ret1+ret2
        if chi=='L':
            ret1 *= 1j
            ret2 *= 1j
        else:
            ret1 *= -1j
            ret2 *= -1j
        if self.structures[3]:
            return ret1
        else:
            return ret1+ret2


class Polynom():
    """ Object representing the polynomial coefficients """
    def __init__(self, nmax, structures):
        """ nmax: maximum power of the ansatz. Powers: pair of lists of powers allowed in the Ansatz, one for each parity sector
        structures: 4-dim boolen vector representing the allowed ansatz. In order this is:
        [(P even, y even), (P even, y odd), (P odd, y even), (P odd, y odd) ]
        """
        self.nmax = nmax
        self.structures = structures

        # Parity-even powers
        self.powersEven = array([n for n in range(nmax+1) if structures[n%2]==1])
        # Parity-odd powers
        self.powersOdd = array([n for n in range(nmax+1) if structures[2+n%2]==1])

        self.len = len(self.powersEven)+len(self.powersOdd)

    def eval(self, y, chi='L', d=0):
        """ Evaluate the list of monomials at a point.
        chi: chirality
        d: number of derivatives """

        # Coefficient deriving from taking derivatives
        ce = array([np.prod(np.arange(p+1-d,p+1)) for p in self.powersEven])
        c0 = array([np.prod(np.arange(p+1-d,p+1)) for p in self.powersOdd])

        monome = array([y**(p-d) if (p-d>=0) else 0 for p in self.powersEven])
        monomo = array([y**(p-d) if (p-d>=0) else 0 for p in self.powersOdd])

        if chi=='L':
            return np.concatenate([monome, 1j*monomo])
        elif chi=='R':
            return np.concatenate([monome, -1j*monomo])
        else:
            raise ValueError("Unknown chirality")



def vectorize(gSqvec, polynomCoefs):
    """ Representation of the ansatz for the S-matrix as a vector """
    return np.concatenate((gSqvec, polynomCoefs), axis=None)


class Smatrix():
    """ Class representing the S-matrix evaluated on the real line """

    def __init__(self, polynom, bsList=[]):
        """
        bsList: list of BoundState instances
        polynom: Polynom class instance
        """
        self.bsList = bsList
        self.polynom = polynom

        self.param = None

    def evaluate(self, y, gSqvec, polynomCoeff):
        """ Evaluate the (vector) S-matrix at a point S given the input for the couplings and the polynomial coefficients
        y: where the S-matrix is evaluated
        gSqvec: numpy array of length n_BS, where n_BS is the number of couplings, representing a list of squared couplings
        polynomCoeff: polynomial coefficients
        """

        if len(gSqvec) != len(self.bsList):
            raise RuntimeError("gSqvec of shape incompatible with number of bound states".format(str(gSqvec.shape)))
        if len(polynomCoeff) != self.polynom.len:
            raise RuntimeError("polynom should be of dimension {}".format(self.polynom.len))

        # Add pole contribution
        ret = np.dot(array([bs.pole(y) for bs in self.bsList], dtype=complex), gSqvec)

        # Add polynomial expansion contribution
        ret += np.dot(polynomCoeff, self.polynom.eval(y))

        return ret


    def linOp(self, y, chi='L', d=0):
        """ Representation of the S-matrix as a vector.
        d: number of derivatives"""

        if len(self.bsList)==0:
            ret1 = array([], dtype=complex)
        else:
            ret1 = array([bs.pole(y,chi) for bs in self.bsList], dtype=complex)

        ret2 = self.polynom.eval(y, chi, d)

        return np.concatenate((ret1, ret2))


# http://cvxopt.org/userguide/coneprog.html#second-order-cone-programming
class Optimizer():

    def __init__(self, grid, S, quiet=False):
        """
        grid: grid of point on the upper half boundary of the disk where the unitarity constraints will be enforced
        S: Smatrix instance
        """
        self.grid = grid
        self.S = S

        solvers.options['show_progress'] = (not quiet)

    def optimize(self, obj, eq_cons=[]):
        """ Minimize a linear functional of the S-matrix, satisfying the constrains |S(s_i)|^2<=1
        obj: linear functional to maximize
        eq_cons: Linear equality constraints
        """
        grid = self.grid

        # Number of quadratic inequality constraints
        n_ineq_cons = len(self.grid)

        # Dimension of the vector space
        d = len(self.S.bsList) + self.S.polynom.len

        if len(obj) != d:
            raise RuntimeError("Objective linear function should have dimension {}".format(d))
        for c in eq_cons:
            if len(c)!=d:
                raise RuntimeError("Equality constraintsldimension {}".format(d))

        # Representation of the S-matrix as a linear operator for each point on the grid
        ops = [self.S.linOp(y) for y in self.grid]

        c = matrix(obj)
        G = [ matrix(array([np.zeros(d), np.real(op), np.imag(op)])) for op in ops ]
        h = [matrix([1.,0.,0.])]*n_ineq_cons

        # Impose positivity of g^2 only (XXX is this right?)
        if self.S.bsList != []:
            G0 = spmatrix(-1., I=np.arange(len(self.S.bsList)), J=np.arange(len(self.S.bsList)), size=(d,d))
            h0 = matrix(np.zeros(d))
        else:
            G0 = None
            h0 = None

        if eq_cons != []:
            # Impose equality constraints
            b = matrix(np.zeros(len(eq_cons)))
            # XXX Can this be made more efficient? Look at on.py
            A = matrix(np.vstack(eq_cons))
        else:
            b = None
            A = None

        res = solvers.socp(c=c, Gq=G, hq=h, Gl=G0, hl=h0, A=A, b=b)
        self.S.param = np.array(res['x']).flatten()
        return res
