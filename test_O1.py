from o1 import *
import pytest
import random

@pytest.mark.skip(reason="Skipping o1 for now")
def test_S():
    """ Test that two ways to compute the S-matrix are equivalent """
    # Number of bound states
    NBS = 4
    mlist = np.random.rand(NBS)
    bsList = array([BoundState(mlist[i]) for i in range(NBS)])
    gSqvec = np.random.uniform(sqrt(2)+0.01, 1.99, NBS)

    nmax = 30
    # Allow all structures
    polynom = Polynom(nmax, [True,True,True,True])

    polynomCoeff = np.random.rand(polynom.len)

    mat = Smatrix(polynom, bsList)

    # Sample in the unit disk
    r = np.random.uniform(0,1, 100)
    theta = np.random.uniform(0, 2*pi, 100)
    ylist = r*np.exp(theta*1j)

    for y in ylist:
        S1 = mat.evaluate(y, gSqvec, polynomCoeff)
        op = mat.linOp(y)
        v = vectorize(gSqvec, polynomCoeff)
        S2 = np.dot(op, v)
        np.testing.assert_almost_equal(S1, S2)


@pytest.mark.skip(reason="Skipping o1 for now")
def test_symmetric():
    # TODO extend this test case
    """ Test that the S-matrix is crossing symmetric when only even powers in the odd powers of y are absent """
    # Number of bound states
    NBS = 4
    mlist = np.random.rand(NBS)
    bsList = array([BoundState(mlist[i]) for i in range(NBS)])
    gSqvec = np.random.uniform(sqrt(2)+0.01, 1.99, NBS)

    nmax = 30
    polynom = Polynom(nmax, [True,False,False,False])

    polynomCoeff = np.random.rand(polynom.len)

    mat = Smatrix(polynom, bsList)

    # Sample in the unit disk
    r = np.random.uniform(0,1, 100)
    theta = np.random.uniform(0, 2*pi, 100)
    ylist = r*np.exp(theta*1j)

    for y in ylist:
        S1 = mat.evaluate(y, gSqvec, polynomCoeff)
        S2 = mat.evaluate(-y, gSqvec, polynomCoeff)
        np.testing.assert_almost_equal(S1, S2)
