import scipy
import json
import dataset
import datetime
from scipy import array
import sys
import numpy as np

# The range where to look for numerical values in the database
tol = 10**-6

class Database():

    # Numpy array objects
    arrays = ['param', 'obj', 'coeff', 'structures', 'phases']

    def __init__(self, dbname="data/coeff.db", tablename="UN3"):
        self.db = dataset.connect('sqlite:///'+dbname)
        self.table = self.db[tablename]

    def insert(self, datadict):
        d = {}
        d["date"] = datetime.datetime.now()

        for k,v in datadict.items():
            if k in self.arrays:
                d[k] = v.tostring()
            else:
                d[k] = v
        self.table.insert(d)


    def getObjList(self, obj, exactQuery={}, approxQuery={}, orderBy="date"):
        """ Get a list of all objects satisfying the query """

        listRes = []

        # Convert bool to int
        queryStrings = []
        for key,value in exactQuery.items():
            if(value==None):
                queryStrings.append("({} IS NULL)".format(key))
            elif(type(value) == bool):
                queryStrings.append("({}={})".format(key,int(value)))
            else:
                queryStrings.append("({}='{}')".format(key,value))

        query = "SELECT {} FROM {}".format(obj, self.table.table)

        if queryStrings != []:
            query += " WHERE ".format(obj, self.table.table) +\
                " AND ".join(queryStrings)+" AND" +\
                " AND ".join(["({} BETWEEN {} AND {})".\
                format(key,value-tol,value+tol) for key,value in approxQuery.items()])+\
                    " ORDER BY {} DESC".format(orderBy)

        for e in self.db.query(query):
            """ param: optimized parameters of the S-matrix """
            """ obj: full objective vector """
            """ coeff: coefficients of the combination of the couplings at the crossing symmetric point which was optimized """

            if obj=='param' or obj=='obj' or obj=='coeff':
                listRes.append(scipy.fromstring(e[obj]))
            else:
                listRes.append(e[obj])
        return listRes

    def get(self):
        """ Get everything """
        # x = self.db.query("SELECT * FROM {}".format(self.table.table))
        # for y in x:
            # print(y['structures'])
            # print(scipy.fromstring(y['structures']), dtype=bool_, count=4)
        return [{k:scipy.fromstring(v) if k in self.arrays else v for (k,v) in x.items()}
                for x in self.db.query("SELECT * FROM {}".format(self.table.table))]
