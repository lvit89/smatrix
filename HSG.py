##############################
# Homogenous Sine-Gordon theory for SU(3), (parity-violating, with anti-particles)
#############################

from o1 import *
import matplotlib.pyplot as plt
from numpy import tanh
from sys import argv, exit

if len(argv) < 3:
    print("{} <sigma> <nmax>".format(argv[0]))
    exit(1)

sigma = float(argv[1])
nmax = int(argv[2])

k = 3

form = "pdf"
quiet = True

# Ansatz for parity-preserving S-matrix with anti-particles (T11 element)
structures = [1, 1, 1, 1]

# Location of resonance in the y plane, given that in the theta plane it's at  sigma + i pi/2
th0 = sigma+1j*pi/k
y0 = yfuntheta(th0)
print("y0={}, f(y0)={}".format(y0, HSG(th0, sigma, k)))

thetamax = 10
gridfactor = 4
gridsize = gridfactor*nmax
# Unitarity is also imposed at negative theta
thgrid = np.linspace(-thetamax, thetamax, gridsize)
grid = np.concatenate((yfunthetaRe(thgrid), yfunthetaRe(1j*pi-thgrid)))

polynom = Polynom(nmax, structures)
# Dimension of vector space
d = polynom.len

S = Smatrix(polynom)
obj = np.zeros(d)
# Maximize S-matrix at crossing symmetric point
# obj[0:2] = [0.3526668,  0.18649888]

obj = -np.imag(S.linOp(y=y0,d=1)) #+ .95*np.imag(S.linOp(y=y0,d=1))
obj = -np.imag(S.linOp(y=0))

# Impose zero at y=y0 (the imaginary part is zero by construction)
eq_cons = [np.real(S.linOp(y0)), np.imag(S.linOp(y0))]
# print(eq_cons)

# NOTE Fix the global phase by fixing the phase at the crossing symmetric point
phase = np.imag(HSG(1j*pi/2, sigma, k))/np.real(HSG(1j*pi/2,sigma,k))
eq_cons += [np.imag(S.linOp(0)) - phase*np.real(S.linOp(0))]

o = Optimizer(grid, S, quiet=quiet)
res = o.optimize(obj, eq_cons)

# print(np.array(res['x']).flatten())

# List of real theta where to plot S-matrix
thlist = np.linspace(-5,5,100)
# Corresponding list of y, when the imaginary part of theta is small from above
ylist = yfunthetaRe(thlist)

fig, ax  = plt.subplots(1,1,sharex=True)
# fig.set_figheight(8)
# fig.set_figwidth(5)

f = array([np.dot(S.linOp(y,'L'),res['x']) for y in ylist])
fth = HSG(thlist, sigma, k)

print("f(y0)={}".format(np.dot(S.linOp(y0),res['x'])))

ax.plot(thlist, np.real(f), label=r"$Re S$")
ax.plot(thlist, np.imag(f), label=r"$Im S$")
ax.plot(thlist, np.absolute(f), label=r"$|S|$")

ax.plot(thlist, np.real(fth), label=r"$Re S^{{\rm th}}$", linestyle='--', linewidth=2)
ax.plot(thlist, np.imag(fth), label=r"$Im S^{{\rm th}}$", linestyle='--', linewidth=2)

ax.legend(loc=4, prop={'size':6})

plt.suptitle(r'HSG, $SU(3)_{}$, $\sigma=${}'.format(k, sigma))
plt.subplots_adjust(wspace=0.1, hspace=0.1)
plt.xlabel(r"$\theta$")

plt.savefig("ReImHSG_k={}_sigma={}.{}".format(k, sigma, form))
