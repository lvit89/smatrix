from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit

ntailsList = (0,1,2)
thetamax = 10

if len(sys.argv)<2:
    print("{} <N>".format(argv[0]))
    sys.exit(1)

N = int(sys.argv[1])

nmaxList = [30,60,100]
nmaxList = 10*np.array(range(10,32,2))

# Location of pole in theta plane
thetap = thetafuns(massGN(N)**2)


rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# params = {'legend.fontsize': 8, 'lines.markersize':2.5, 'lines.marker':"o"}
# plt.rcParams.update(params)
plt.style.use('ggplot')

reps = [Repr.ANTI, Repr.SING]
mb = [massGN(N)]*2
bsList = [BoundState(mb[i], N, reps[i]) for i in range(len(mb))]

sat = {ntails:[[],[],[]] for ntails in ntailsList}
maxg = {ntails:[] for ntails in ntailsList}

thetaList = np.linspace(0.01, 10, 200)
ylist = yfunthetaRe(thetaList)

for ntails in ntailsList:
    for nmax in nmaxList:
        print("nmax ={}".format(nmax))

        gridsize = 3*nmax

        thetagrid = np.linspace(0, thetamax, gridsize)
        grid = yfunthetaRe(thetagrid)

        S = Smatrix(bsList, N, nmax, ntails)

        obj = np.zeros(S.size)
        obj[0] = -1.

        cons = []
        # for i in (2,):
            # x = S.linOp(y=y0)[i]
            # cons.append(np.real(x))

        o = Optimizer(grid, S)
        res = o.optimize(obj, cons=cons)

# Check that S-matrix is zero where requested
        # np.testing.assert_almost_equal(np.dot(S.linOp(y0), res['x'])[2], 0.)

        f = np.squeeze(array([np.dot(S.linOp(y), res['x']) for y in ylist], dtype=complex)).transpose()

        # "Unitarity saturation"
        for i in range(3):
            sat[ntails][i].append(np.absolute(f[i]).mean())

        maxg[ntails].append(-res['primal objective'])


style = {0:"--", 2:"-"}

for ntails in (0,2):
    for i, rep in enumerate(list(Repr)):
        plt.plot(nmaxList, sat[ntails][i], label="R={}, ntails={}".format(str(rep), ntails), linestyle=style[ntails])
plt.axhline(1.)

plt.legend(loc=4)
plt.title(r"$\theta_{{\rm max}} = {}, N = {}$".format(max(thetaList), N))
plt.xlabel(r"$n_{\rm max}$")
plt.ylabel(r"$\frac{1}{\theta_{\rm max}} \int_0^{\theta_{\rm max}} d \theta |S(\theta)|$")
plt.savefig("convergence_N={}.pdf".format(N))
plt.clf()

for ntails in (0,2):
    plt.plot(nmaxList, maxg[ntails], label="tail={}".format(ntails))

# Analytic coupling for N=7
plt.axhline(36.0636)

plt.legend(loc=4)
plt.title(r"$N = {}$".format(N))
plt.xlabel(r"$n_{\rm max}$")
plt.ylabel(r"$g_{\rm max}$")
plt.savefig("maxg_N={}.pdf".format(N))
