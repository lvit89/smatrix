from on import *
import matplotlib.pyplot as plt
from analytic import *
import sys

group = ON
d = group.d
Repr = group.Repr

quiet = True
ntails = 2

thetamax = 4

# Number of points on the unitarity grid over number of states
gridfactor = 2

argv = sys.argv
if len(argv)<3:
    print("{} <N> <nmax>".format(argv[0]))
    sys.exit(1)


N = int(sys.argv[1])
nmax = int(sys.argv[2])

bsList = []

gridsize = gridfactor*nmax
thetagrid = np.linspace(0, thetamax, gridsize)
grid = yfunthetaRe(thetagrid)

S = Smatrix(bsList, N, nmax, group, ntails, quiet=quiet)

# Impose zero at y=y0 (the imaginary part is zero by construction)
# eq_cons = [np.real(polynom.eval(y0))]
eq_cons = []

o = Optimizer(grid, S)

# List of real theta where to check unitarity
thlist = np.linspace(0,thetamax,1000)
# Corresponding list of y
ylist = yfunthetaRe(thlist)
ops = [S.linOp(y) for y in ylist]

samples = 1000
nzerosVec = np.zeros(samples)
satVec = np.zeros(samples)

obj = np.zeros(S.size)

for i in range(samples):
    # Maximize random functional of the S-matrix at y=0
    coeff = np.random.normal(size=3)
    obj = np.einsum("i,ij -> j", coeff, S.linOp(y=0))

    # Maximize random combination of asymptotic terms
    # obj[-3:] = np.random.normal(size=3)


    res = o.optimize(obj, eq_cons)
    # print(res['primal objective'])

    f = array([np.dot(op, res['x']) for op in ops], dtype=complex)
    imagf = np.imag(f).transpose()[0,0]

    nzeros = sum(abs(imagf-np.roll(imagf,1)))/2
    # if nzeros<1.9 and nzeros>0.1:
        # print("nzeros={}, obj={}".format(nzeros, obj[:2]))

    nzerosVec[i] = nzeros

    sat = np.mean(np.abs(f))
    satVec[i] = sat

    if sat>0.99 and nzeros>0.2:
        print("sat:{}, coeff=[{},{},{}]".format(sat, coeff[0],coeff[1],coeff[2]))

# plt.hist(sat)
# plt.savefig("sat_hist.pdf")
# sys.exit(0)

plt.scatter(nzerosVec, satVec)
plt.xlabel("nzeros")
plt.ylabel(r"$\bar{|S|}$")
plt.savefig("nzeros_hist.pdf")

# # List of real theta where to plot S-matrix
# thlist = np.linspace(-10,10,100)
# # Corresponding list of y, when the imaginary part of theta is small from above
# ylist = thetaToY(thlist)
# f = array([np.dot(S.linOp(y),res['x']) for y in ylist], dtype=complex)

# plt.plot(thlist, np.real(f), label="Re S")
# plt.plot(thlist, np.imag(f), label="Im S")
# plt.plot(thlist, np.absolute(f), label="|S|")
# plt.ylim(-1.1, 1.1)
# plt.xlabel(r"$\theta$")
# plt.title(r'HSG, $SU(3)_2$')
# plt.legend()
# plt.savefig("HSG_exp.pdf")
