from on import *
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from numpy import cos, sin, exp, log, sqrt, pi, tan
from scipy.special import gamma
from sys import argv, exit
import database

plot = True

# Maximum imaginary value of theta where to check Yang Baxter
imax = 0.1

# Plot the irreducible representations instead of (t1, u1, t2, u2)
show = "Irrep"
# Plot (t1, u1, t2, u2)
# show = "Comps"

if show == "Irrep":
    irrep = True
else:
    irrep = False

groupClass = UN3
table = "UN3"
# dbname = "data/coeffRemote.db"
dbname = "data/coeff.db"

form = "png"
thetamax = 5


iid = None

if len(argv) == 2:
    # print("{} <id> <phi>".format(argv[0]))
    iid = int(argv[1])
    print("id:", iid)
    # print("{} <id>".format(argv[0]))
    # exit(1)



# phi = float(argv[2])
phi = 0



# rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
plt.style.use('ggplot')

db = database.Database(dbname=dbname, tablename=table)

for row in db.get():
    N = row['N']
    param = row['param']
    nmax = row['nmax']
    ntails = row['ntails']
    Id = row['id']

    if iid!=None and iid!=Id:
        continue

    group = groupClass(N)
    d = group.d

    if show=="Irrep":
        Repr = group.Irrep
    else:
        Repr = group.Comps

    S = Smatrix(bsList=[], nmax=nmax, group=group, ntails=ntails)

    S.param = param

    # Multiply by global phase
    c = cos(phi)
    s = sin(phi)
    rot = array([[c,0,s,0],[0,c,0,s],[-s,0,c,0],[0,-s,0,c]])
    mat = np.kron(np.eye(int(param.size/4)), rot)

    S.param = np.dot(mat, S.param)

    maxYB = np.max(S.checkYB(imax=imax)[2], axis=0)
    meanYB = np.mean(S.checkYB(imax=imax)[2], axis=0)
    stdYB = np.std(S.checkYB(imax=imax)[2], axis=0)
    print("id={}, max:{}".format(Id, maxYB))
    print("mean: ", meanYB)
    print("std: ", stdYB)

    if not plot:
        continue

#############################
# Plot at physical energy
############################

    thetaList = np.linspace(0.01, thetamax, 100)
    ylist = yfunthetaRe(thetaList)
    ops = [S.linOp(y, irrep=irrep) for y in ylist]


    f = np.squeeze(array([np.dot(op, S.param) for op in ops], dtype=complex)).transpose()

    h = int(len(Repr)/2)
    fig, ax  = plt.subplots(h,2,sharex=True)
    fig.set_figheight(8)
    fig.set_figwidth(7)

    for i, rep in enumerate(list(Repr)):
        idx = (i%h, int(i/h))
        ax[idx].plot(thetaList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)))
        ax[idx].plot(thetaList, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)))
        ax[idx].plot(thetaList, np.absolute(f[i]), label=r"$|S|_{{\rm {}}}$".format(str(rep)))

        ax[idx].legend(loc=4, prop={'size':6})
        ax[idx].set_ylim(-1.05, 1.05)

    fig.suptitle(r"table={}, id={}, phi={}, maxYB={}".format(table,Id,phi,maxYB))
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.xlabel(r"$\theta$")
    # plt.savefig("plots/table={}_id={}_phi={}_ReTh.{}".format(table, Id, phi, form))
    plt.savefig("plots/table={}_id={}_ReTh.{}".format(table, Id, form))
    plt.close()

#############################
# Plot at imaginary theta
############################

    imthList = np.linspace(0.01, pi-0.01, 100)
    ylist = yfuntheta(1j*imthList)
    ops = [S.linOp(y) for y in ylist]

    f = np.squeeze(array([np.dot(S.linOp(y), S.param) for y in ylist], dtype=complex)).transpose()

    h = int(len(Repr)/2)
    fig, ax  = plt.subplots(int(h/2),2,sharex=True)
    fig.set_figheight(8)
    fig.set_figwidth(7)

    for i, rep in enumerate(list(Repr)):

        if i >=4:
            break
# Only plot left component

        idx = (i%2, int(i/2))
        ax[idx].plot(imthList, np.real(f[i]), label=r"$Re S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)
        ax[idx].plot(imthList, np.imag(f[i]), label=r"$Im S_{{\rm {}}}$".format(str(rep)), linestyle='--', linewidth=2)

        ax[idx].legend(prop={'size':6})
        ax[idx].set_ylabel(r"$S_{{\rm {}}}$".format(str(rep)))

    fig.suptitle(r"id={}, phi={}".format(Id,phi))
    plt.subplots_adjust(wspace=0.1, hspace=0.1)
    plt.xlabel(r"$Im \theta$")

    plt.savefig("plots/table={}_id={}_ImTh.{}".format(table, Id, form))
    plt.close()
